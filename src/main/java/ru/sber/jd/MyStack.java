package ru.sber.jd;

import java.security.PrivateKey;

public class MyStack {

    static class Element {
        Integer value;
        Element next;

        Element(Integer value, Element next) {
            this.value = value;
            this.next = next;
        }

        @Override
        public boolean equals(Object o) {

            if (this.value.intValue() == ((Element) o).value.intValue() && this.next == (((Element) o).next)) {
                return true;
            } else {
                return false;
            }


        }

    }

    static class MinElement {
        Element element;
        MinElement next;

        MinElement(Element element, MinElement next) {
            this.element = element;
            this.next = next;
        }

    }


    private int size = 0;
    private MinElement min;
    private Element current;


    public MyStack() {
    }

    public String printStackMin() {
        MinElement me = this.min;
        String result = "";

        if (this.current != null) {
            result = result + me.element.value;

            while ((me = me.next) != null) {
                result = result + " " + me.element.value;
            }
        }
        return (result == "" ? null : result);
    }

    public int getSize() {
        return this.size;
    }

    public Integer getCurrent() {
        if (this.current == null) {
            return null;
        } else {
            return this.current.value;
        }
    }

    public Integer getMin() {
        if (this.min != null) {
            return this.min.element.value;
        } else {
            return null;
        }
    }

    public void remove() {

        if (this.current.next == null) {
            this.size--;
            this.current = null;
            this.min = null;
        } else {

            //если удаляемый объект минимальный
            if (this.current.equals(this.min.element)) {
                MinElement minTemp = this.min.next;
                this.min = new MinElement(minTemp.element, minTemp.next);
            }

            this.size--;
            Element temp = this.current.next;
            this.current = new Element(temp.value, temp.next);

        }
    }


    public void add(Integer value) {
        //если это первый элемент, то записываем в текущий
        if (this.current == null) {
            this.size++;
            this.current = new Element(value, null);
            this.min = new MinElement(this.current, null);

            //если не первый
        } else {
            this.size++;
            Element temp = this.current;
            this.current = new Element(value, temp);

            //сравниваем с минимальным
            if (value < this.min.element.value) {
                MinElement minTemp = min;
                this.min = new MinElement(this.current, minTemp);
            }

        }


    }


    @Override
    public String toString() {
        String result = "";
        Element e;

        if (this.current == null) {
            return null;
        } else {
            result = "" + this.current.value;
            e = this.current;
            while ((e = e.next) != null) {
                result = result + " " + e.value;
            }


        }


        return result;
    }


}


