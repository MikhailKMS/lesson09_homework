import ru.sber.jd.MyStack;

public class Main {

    public static void main(String[] args) {

        MyStack myStack = new MyStack();

        myStack.add(-1);
        myStack.add(-5);
        myStack.add(-3);
        myStack.add(-6);
        myStack.add(-7);

        System.out.println("Стэк: " + myStack);
        System.out.println("Стэк минимальных элементов: " + myStack.printStackMin());
        System.out.println("Количество значений в Стэке = " + myStack.getSize());
        System.out.println("Текущий элемент = " + myStack.getCurrent());
        System.out.println("Значение минимального элемента в Стэке = " + myStack.getMin());

        myStack.remove();
        myStack.remove();
        myStack.remove();
        myStack.remove();
        myStack.add(-7);
        myStack.remove();

        System.out.println("\nСтэк: " + myStack);
        System.out.println("Стэк минимальных элементов: " + myStack.printStackMin());
        System.out.println("Количество значений в Стэке = " + myStack.getSize());
        System.out.println("Текущий элемент = " + myStack.getCurrent());
        System.out.println("Значение минимального элемента в Стэке = " + myStack.getMin());



    }



}
